const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Role = db.role;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

// create account
exports.register = (req, res) => {
  const user = new User({
    username: req.body.username,
    password: bcrypt.hashSync(req.body.password, 8)
  });
  
  user.save((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (req.body.roles) {
      Role.find(
        {
          name: { $in: req.body.roles }
        },
        (err, roles) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          user.roles = roles.map(role => role._id);
          user.save(err => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }
            res.send({ 
                message: "account created successfully",
                id: user._id,
                username: user.username,
            });
          });
        }
      );
    } else {
      Role.findOne({ name: "user" }, (err, role) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        user.roles = [role._id];
        user.save(err => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          res.send({ 
            message: "account created successfully",
            id: user._id,
            username: user.username,
          });
        });
      });
    }
  });
};

// get list all data users
exports.getAllUsers = (req, res) => {
    User.find()
    .populate("roles", "-__v")
    .exec((err, result) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }
        res.status(200).json({
            message: "list all user data",
            data: result
        });
    })
    
};