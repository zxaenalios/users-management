const db = require("../models");
const User = db.user;

// get data user by id
exports.getDataUser = (req, res) => {
    const id = req.params.id;
    User.findById(id)
    .then(result => {
        if (!result){
          return res.status(400).end();
        }
        return res.status(200).send(result)
      })
    .catch(err => res.status(400).send(err));
};

// update data user by id
exports.updateDataUser = (req, res) => {
    const id = req.params.id;
    const username = req.body.username;
    const phone = req.body.phone;
    const address = req.body.address;
    User.findByIdAndUpdate(
        id, 
        {
            username: username,
            phone: phone,
            address: address
        }     
    )
    .then(result => {
        if (!result){
          return res.status(400).end();
        }
        return res.status(200).send({
            message: "user data changed successfully",
            _id: result.id,
            username: username,
            phone: phone,
            address: address
        })
      })
    .catch(err => res.status(400).send(err));
};