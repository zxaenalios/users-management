const { authJwt, verifyRegister } = require("../middlewares");
const controller = require("../controllers/admin.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  // register route
  app.post(
    "/api/admin/register",
    [
      authJwt.verifyToken, 
      authJwt.isAdmin,
      verifyRegister.checkDuplicateUsername,
    ],
    controller.register
  );
  
  // get all users data route
  app.get(
    "/api/admin/users",
    [
      authJwt.verifyToken, 
      authJwt.isAdmin,
    ],
    controller.getAllUsers
  );
};